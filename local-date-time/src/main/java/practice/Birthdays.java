package practice;

import java.sql.SQLOutput;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Birthdays {

  public static void main(String[] args) {

    int day = 31;
    int month = 12;
    int year = 1990;


    System.out.println(collectBirthdays(year, month, day));

  }

  public static String collectBirthdays(int year, int month, int day) {

    LocalDate today = LocalDate.now();
    LocalDate birthday = LocalDate.of(year, month, day);

    DateTimeFormatter formatter =
      DateTimeFormatter.ofPattern("dd.MM.yyyy - EE ").localizedBy(new Locale("us"));

    String text = "";
    int i = 0;


    while (birthday.isBefore(today) || birthday.isEqual(today)) {
      String birthdayDate = formatter.format(birthday);
      text = text + i + " - " + birthdayDate + System.lineSeparator();
      birthday = birthday.plusYears(1);
      i++;
      if (birthday.isAfter(today)) {
        break;


      }


      //TODO реализуйте метод для построения строки в следующем виде
      //0 - 31.12.1990 - Mon
      //1 - 31.12.1991 - Tue


    }
    return text;
  }
}

