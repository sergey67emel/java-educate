package practice;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Periods {

  private static long allDays;
  private static long months;
  private static long years;
  private static long days;

  // реализуйте вывод разницы между датами, используя класс Period
  public static String getPeriodFromBirthday(LocalDate firstDate, LocalDate secondDate) {

    String date = "1995/05/23";
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
    LocalDate localDate = LocalDate.parse(date, formatter);

    secondDate = LocalDate.now();
    firstDate = localDate;


    allDays = firstDate.until(secondDate, ChronoUnit.DAYS);

    years = firstDate.until(secondDate, ChronoUnit.YEARS);
    months = (allDays % 365) / 30;
    days = allDays - (years * 365) - (months * 30);


    return "years:" + years + "," + " "
      + "months:" + months + "," + " "
      + "days:" + days;

  }


}
