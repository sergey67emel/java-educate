package practice.string;

import java.util.Scanner;

public class SequentialWordsNumbers {
  public static void main(String[] args) {
    while (true) {
      System.out.println("Введите текст ");
      String text = new Scanner(System.in).nextLine();
      System.out.println(sequentialWordsNumbers(text));
    }
  }

  public static String sequentialWordsNumbers(String text) {
    StringBuilder sb = new StringBuilder(text);
    int count = 0;

    if (sb.charAt(0) != ' ') {
      sb.insert(0, " ");
    }
    while (sb.indexOf(" ") != -1) {
      sb.replace(sb.indexOf(" "), sb.indexOf(" ") + 1, "(" + (++count) + ")");
      if (sb.indexOf(" ") == -1) {
        return String.valueOf(sb);
      }


    }

    return text;
  }
}

