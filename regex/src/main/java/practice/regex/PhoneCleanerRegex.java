package practice.regex;

import java.util.Scanner;

public class PhoneCleanerRegex {

  public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);
    while (true) {
      String input = scanner.nextLine();
      if (input.equals("0")) {
        scanner.close();
        break;
      }

      String regexCleaning = "[^0-9]";
      String phoneNumbers = input.replaceAll(regexCleaning, "");

      String regexShortNumber = "[0-8][0-9]{1,8}";
      String regexLongNumber = "[0-9]{12,}";

      if (phoneNumbers.matches(regexShortNumber) || phoneNumbers.matches(regexLongNumber)) {
        System.out.println("Неверный формат номера");
        break;
      }

      String regex = "[7][0-9]{10}";
      if (phoneNumbers.matches(regex)) {
        System.out.print(phoneNumbers);
      } else {
        String[] number = phoneNumbers.split("");
        for (int i = 0; i < phoneNumbers.length(); i++)
          if (number[0].equals("8")) {
            System.out.print(phoneNumbers.replaceFirst("8", "7"));
            break;
          }

        String regexFirstNumber = "[^7-8][0-9]{10}";
        if (phoneNumbers.matches(regexFirstNumber)) {
          System.out.println("Неверный формат номера");
          break;
        }
      }
      String regexAdditionNumber = "[9][0-9]{9}";
      if (phoneNumbers.matches(regexAdditionNumber)) {
        StringBuilder firstNumber = new StringBuilder(phoneNumbers);
        firstNumber.insert(0, "7");
        System.out.print(firstNumber);

      }


    }
  }
}

// TODO:напишите ваш код тут, результат вывести в консоль.



