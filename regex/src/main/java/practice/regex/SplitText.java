package practice.regex;

import java.util.Scanner;
import java.util.regex.Pattern;

public class SplitText {

  public static void main(String[] args) {
    System.out.println("Введите текст");
    String text = new Scanner(System.in).nextLine();
    System.out.println(splitTextIntoWords(text));

  }

  public static String splitTextIntoWords(String text) {
    String regex = "[\\d+,;:.-]+";
    String value = text.replaceAll(regex," ");
    Pattern pattern = Pattern.compile("\\s+");
      String[] words = pattern.split(value);
      StringBuilder newText = new StringBuilder();
    for (String word : words) {
       newText.append(word).append('\n');
    }
      //TODO реализуйте метод



    return newText.toString().trim();
  }
}
