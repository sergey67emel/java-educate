package practice;

import java.util.ArrayList;
import java.util.Scanner;

public class TodoList {
  ArrayList<String> todoList = new ArrayList<>();

  public void add(String todo) {
    todoList.add(todo);
    System.out.println("Добавлено дело" + "\n" + "\"" + todo + "\"");
    // TODO: добавьте переданное дело в конец списка
  }

  public void add(int index, String todo) {
    if (index >= 0 && index < todoList.size()) {
      todoList.add(index, todo);
      System.out.println("Добавлено дело" + "\n" + "\"" + todo + "\"");
    } else {
      if (index < 0) {
        todoList.add(todo);

      }
      // TODO: добавьте дело на указаный индекс,
      //  проверьте возможность добавления
    }
  }

  public void edit(int index, String todo) {
    if (index >= 0 && index < todoList.size()) {
      System.out.println("Дело" + " " + "\"" + todoList.get(index) + "\"" + "заменено на" + " " + "\"" + todo + "\"");
      todoList.set(index, todo);

    } else {
      System.out.println();
    }
    // TODO: заменить дело на index переданным todo индекс,
    //  проверьте возможность изменения
  }

  public void delete(int index) {
    if (index >= 0 && index < todoList.size()) {
      System.out.println("Дело" + " " + "\"" + todoList.get(index) + "\"" + "удалено");
      todoList.remove(index);

    } else {
      if (index < 0 || index > todoList.size() - 1) {
        System.out.println("Дело с таким номером не существует.");
      }
    }
    // TODO: удалить дело находящееся по переданному индексу,
    //  проверьте возможность удаления дела
  }

  public ArrayList<String> getTodos() {
    StringBuilder stringBuilder = new StringBuilder();
    for (int i = 0; i < todoList.size(); i++) {
      if (i == todoList.size() - 1) {
        stringBuilder.append(i + " - " + todoList.get(i));
        continue;
      }
      stringBuilder.append(i + " - " + todoList.get(i) + "\n");
      System.out.println(stringBuilder);
    }
    return todoList;
  }
}


// TODO: вернуть список дел




