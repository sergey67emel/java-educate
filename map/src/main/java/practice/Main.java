package practice;

import java.util.*;


public class Main {
  private static  PhoneBook phoneBook = new PhoneBook();

  public static void main(String[] args) {

    String regexName = "[а-яА-Я]+";
    String regexPhone = "([\\d]{11}(, )?){1,2}";

    while (true) {
      System.out.println("Для поиска абонента введите имя или телефон, для вывода всего списка - команду List");
      Scanner scanner = new Scanner(System.in);
      String input = scanner.nextLine();

      if (input.matches(regexName)) {
        processingName(input);
        continue;
      }

      if (input.matches(regexPhone)) {
        processingPhone(input);
        continue;
      }

      if (input.equals("List")) {
        phoneBook.getAllContacts();
        continue;
      }
      System.out.println("Неверный формат ввода!");
    }
  }

  public static void processingName(String name) {
    Scanner scanner = new Scanner(System.in);
    Set<String> contacts = phoneBook.getContactByName(name);

    if (contacts.isEmpty()) {
      System.out.println("Такого имени нет в телефонной книге. Введите номер телефона для абонента " + name);
      String input = scanner.nextLine();
      phoneBook.addContact(input, name);
      Set<String> contacts2 = phoneBook.getContactByName(name);
      System.out.println(contacts2.toString().replaceAll("[\\[\\]]", ""));
    } else {
      System.out.println(contacts.toString().replaceAll("[\\[\\]]", ""));
    }
  }

  public static void processingPhone(String phone) {
    Scanner scanner = new Scanner(System.in);
    String contact = phoneBook.getContactByPhone(phone);
    if ("".equals(contact)) {
      System.out.println("Такого номера нет в телефонной книге. Введите имя абонента для номера " + phone);
      String input = scanner.nextLine();
      phoneBook.addContact(phone, input);
      System.out.println(contact);
    }
  }
}









