package practice;

import java.util.*;

public class PhoneBook {

  public static final String REGEX_NAME = "[а-яА-Я]+";
  public static final String REGEX_PHONE = "([\\d]{11}(, )?){1,2}";

  Map<String, String> phoneBook = new TreeMap<>();

  public void addContact(String phone, String name) {
    if (!phone.matches(REGEX_PHONE)) {
      System.out.println("Передан неверный формат телефонного номера");
      return;
    }

    if (!name.matches(REGEX_NAME)) {
      System.out.println("Передан неверный формат Имени абонента");
      return;
    }

    if (!phoneBook.containsValue(phone) && !phoneBook.containsKey(name)) {
      phoneBook.put(name, phone);
      System.out.println("Контакт сохранён:");
      return;
    }

    if (phoneBook.containsValue(phone)) {
      for (String key : phoneBook.keySet()) {
        if (phoneBook.get(key).equals(phone)) {
          phoneBook.remove(key);
          phoneBook.put(name, phone);
          System.out.println("Контакт перезаписан:");
          return;
        }
      }
    } else if (phoneBook.containsKey(name)) {
      for (String key : phoneBook.keySet()) {
        if (key.equals(name)) {
          String phone1 = phoneBook.get(key);
          phone = String.join(", ", phone1, phone);
          phoneBook.put(name, phone);
          System.out.println(name + " - " + phone);
          return;
        }
      }
    }
  }
  // проверьте корректность формата имени и телефона
  // (рекомедуется написать отдельные методы для проверки является строка именем/телефоном)
  // если такой номер уже есть в списке, то перезаписать имя абонента

  public String getContactByPhone(String phone) {

    String telByPhone = "";
    String regex = "([\\d]{11}[, ]*){2}";

    for (String key : phoneBook.keySet()) {
      if (phoneBook.get(key).equals(phone)) {
        telByPhone = key + " - " + phone;
        System.out.println(telByPhone);
      }
    }
    for (String number : phoneBook.values()) {
      if (number.matches(regex)) {
        String[] twinTelPhone = number.split(", ");
        String phone1 = twinTelPhone[0];
        String phone2 = twinTelPhone[1];
        if (phone1.equals(phone) || phone2.equals(phone)) {
          phone = number;
          for (String key2 : phoneBook.keySet()) {
            if (!phoneBook.get(key2).equals(phone)) {
              continue;
            } else if (phoneBook.get(key2).equals(phone)) {
              telByPhone = key2 + " - " + phone;
              System.out.println(telByPhone);
              return telByPhone;
            } else {
              return "";
            }
          }
        }
      }
    }
    return telByPhone;
  }

  // формат одного контакта "Имя - Телефон"
  // если контакт не найдены - вернуть пустую строку

  public Set<String> getContactByName(String name) {
    Set<String> setPhoneBook = new TreeSet<>();
    String telByName;
    if (phoneBook.containsKey(name)) {
      telByName = String.join(" - ", name, phoneBook.get(name));
      setPhoneBook.add(telByName);
    } else {
      return new TreeSet<>();
    }

    return setPhoneBook;
  }

  // формат одного контакта "Имя - Телефон"
  // если контакт не найден - вернуть пустой TreeSet

  public Set<String> getAllContacts() {
    Set<String> allContacts = new TreeSet<>();
    if (!phoneBook.isEmpty()) {
      for (Map.Entry<String, String> entry : phoneBook.entrySet()) {
        allContacts.add(entry.getKey() + " - " + entry.getValue());
      }
    } else {
      return new TreeSet<>();
    }
    System.out.println(allContacts.toString().replaceAll("[\\[\\]]", ""));
    return allContacts;

  }
}

// формат одного контакта "Имя - Телефон"
// если контактов нет в телефонной книге - вернуть пустой TreeSet


// для обхода Map используйте получение пары ключ->значение Map.Entry<String,String>
// это поможет вам найти все ключи (key) по значению (value)
    /*
        for (Map.Entry<String, String> entry : map.entrySet()){
            String key = entry.getKey(); // получения ключа
            String value = entry.getValue(); // получения ключа
        }
    */




