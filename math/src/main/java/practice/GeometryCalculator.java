package practice;

public class GeometryCalculator {

  // если значение radius меньше 0, метод должен вернуть -1
  public static double getCircleSquare(double radius) {
    if (radius >= 0) {
      return Math.PI * radius * radius;
    } else {
      return -1;
    }
  }

  // если значение radius меньше 0, метод должен вернуть -1
  public static double getSphereVolume(double radius) {
    if (radius >= 0) {
      return (4.0 / 3) * Math.PI * (Math.pow(radius, 3));
    } else {
      return -1;
    }
  }

  public static boolean isTrianglePossible(double a, double b, double c) {
    if (a + b > c && a + c > b && c + b > a) {
      return true;
    } else {
      return false;
    }
  }


  // перед расчетом площади рекомендуется проверить возможен ли такой треугольник
  // методом isTrianglePossible, если невозможен вернуть -1.0
  public static double getTriangleSquare(double a, double b, double c) {
    if (GeometryCalculator.isTrianglePossible(a, b, c)) {
      double p = (a + b + c) / 2;
      return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    } else {
      return -1.0;
    }


  }
}

