package practice;

import net.sf.saxon.functions.Sum;

import java.util.Scanner;

public class TrucksAndContainers {


  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    //получение количество коробок от пользователя

    int boxes = scanner.nextInt();
    int containers = 0;
    int trucks = 0;


    if (boxes == 0) {
      System.out.println(" " + " Необходимо:" + '\n' + "" + "грузовиков - " + (trucks) + " шт." +
        '\n' + "" + "контейнеров - " + "" + (containers) + " шт.");
    }

    if (boxes != 0) {
      for (int i = 1; i <= boxes; i++) {
        if (i == 1) {
          System.out.println("Грузовик: " + i);
          System.out.println("\tКонтейнер: " + i);
          System.out.println("\t\tЯщик: " + i);
          continue;
        }
        System.out.println("\t\tЯщик: " + i);
        if (i % 324 == 0) {
          System.out.println("Грузовик: " + (i / 324 + 1));   // количество ящиков в одном грузовике: 12 * 27 = 324 шт.
        }

        if (boxes < 28) {
          continue;
        } else if (i % 27 == 0) {
          System.out.println("\tКонтейнер: " + (i / 27 + 1));
        }
      }
      for (int k = 1; k <= boxes; k++) {
        containers = k % 27 == 0 ? k / 27 : k / 27 + 1;
      }
      if (containers != 0) {
        for (int c = 1; c <= containers; c++) {
          trucks = c % 12 == 0 ? c / 12 : c / 12 + 1;
        }
      }
      System.out.println("Необходимо:" + '\n' + "грузовиков - " + (trucks) + " шт." +
        '\n' + "контейнеров - " + (containers) + " шт.");
    }
  }
}

// TODO: вывести в консоль коробки разложенные по грузовикам и контейнерам
// пример вывода при вводе 2
// для отступа используйте табуляцию - \t

        /*
        Грузовик: 1
            Контейнер: 1
                Ящик: 1
                Ящик: 2
        Необходимо:
        грузовиков - 1 шт.
        контейнеров - 1 шт.
        */


