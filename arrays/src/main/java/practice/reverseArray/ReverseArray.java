package practice.reverseArray;

public class ReverseArray {


  public static String[] reverse(String[] strings) {
    String[] temp = new String[strings.length];

    for (int i = strings.length - 1, j = 0; i >= 0; j++, i--) {

      temp[j] = strings[i];
    }
    for (int i = 0; i < strings.length; i++) {
      strings[i] = temp[i];
    }

    return strings;
  }

}


//TODO: Напишите код, который меняет порядок расположения элементов внутри массива на обратный.





