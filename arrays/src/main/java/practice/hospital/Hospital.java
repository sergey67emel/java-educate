package practice.hospital;

import java.util.Arrays;

public class Hospital {

  public static float[] generatePatientsTemperatures(int patientsCount) {
    float[] patientsTemperatures = new float[patientsCount];
    for (int i = 0; i < patientsCount; i++) {
      patientsTemperatures[i] = Math.round(((float) (Math.random() * 8 + 32)) * 10) / (float) 10.0;
      System.out.println(patientsTemperatures[i]);
    }
    return patientsTemperatures;
  }

  public static String getReport(float[] temperatureData) {

    int normalTemperature = 0;
    float sum = 0;
    for (int j = 0; j < temperatureData.length; j++) {
      if (temperatureData[j] >= 36.2 && temperatureData[j] < 37) {
        normalTemperature++;
      }
    }
    for (float num : temperatureData) {
      sum = sum + num;
    }
    float averageTemperature = Math.round((sum / temperatureData.length) * 100) / (float) 100.0;
    String listTemperatures = Arrays.toString(temperatureData)
      .replaceAll("\\[|\\]|,", "");

    String report =
      "Температуры пациентов: " + listTemperatures +
        "\nСредняя температура: " + averageTemperature +
        "\nКоличество здоровых: " + normalTemperature;
    return report;
  }
}
 /*
        TODO: Напишите код, который выводит среднюю температуру по больнице,количество здоровых пациентов,
            а также температуры всех пациентов.
            Округлите среднюю температуру с помощью Math.round до 2 знаков после запятой,
            а температуры каждого пациента до 1 знака после запятой
        */
