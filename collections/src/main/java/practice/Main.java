package practice;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
  private static TodoList todoList = new TodoList();
  static ArrayList<String> arrayListTodo = todoList.getTodos();

  public static void main(String[] args) {


    System.out.println("Выберите действие и введите одну из комманд: \n ADD \n EDIT \n DELETE \n LIST ");
    Scanner scanner = new Scanner(System.in);


    while (true) {
      String command = scanner.nextLine();
      String[] words = command.split(" ");
      String typeCommand = words[0];
      StringBuilder sb = new StringBuilder();

      switch (typeCommand) {
        case "LIST":
          if (typeCommand.equals("LIST")) {
            System.out.println(arrayListTodo.toString().replaceAll("(^\\[|\\]$)", ""));
          }
          break;

        case "ADD":
          if (typeCommand.equals("ADD") && words[1].matches("^[a-z]+")) {
            for (int n = 1; n < words.length; n++) {
              sb.append(words[n] + " ");
              continue;
            }
            todoList.add(sb.toString());
            break;
          }
          if (typeCommand.equals("ADD") && words[1].matches("^[0-9]+")) {
            int index = Integer.parseInt(words[1]);
            for (int j = 2; j < words.length; j++) {
              sb.append(words[j] + " ");
            }
            todoList.add(index, sb.toString());
            break;
          }

        case "EDIT":
          if (typeCommand.equals("EDIT") && words[1].matches("^[0-9]+")) {
            int index = Integer.parseInt(words[1]);
            for (int i = 2; i < words.length; i++) {
              sb.append(words[i] + " ");
            }
            todoList.edit(index, sb.toString());
            break;
          }

        case "DELETE":
          if (typeCommand.equals("DELETE") && words[1].matches("[0-9]+")) {
            int index = Integer.parseInt(words[1]);
            todoList.delete(index);
            break;
          }
      }
    }
  }
}

// TODO: написать консольное приложение для работы со списком дел todoList


